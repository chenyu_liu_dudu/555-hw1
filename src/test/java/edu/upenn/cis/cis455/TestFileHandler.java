package edu.upenn.cis.cis455;

import edu.upenn.cis.cis455.m1.handling.FileHandler;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestFileHandler {


    @Test
    public void testIsDirectoryWithIndex() {
        assertFalse(FileHandler.isDirectoryWithIndex(System.getProperty("user.dir")));
    }

    @Test
    public void testIsDirectoryNoIndex() {
        assertTrue(FileHandler.isDirectoryNoIndex(System.getProperty("user.dir") ));
    }


}
