package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.helper.HelperFunction;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.junit.Assert.*;

public class TestHelperFunction {

    Map<String, String> invalidHeaders = Map.of("asfsaf", "asfd", "Accept", "text/html");

    Map<String, String> validHeaders = Map.of("Accept-Encoding", "gzip, deflate", "Accept", "text/html");

    @Test
    public void testPathMatching() {
        boolean matched1 = HelperFunction.checkPatternMatch("/hey/how/are/you", "/*/how/are/*");
        boolean matched2 = HelperFunction.checkPatternMatch("/hey/how/are/you", "/*/*/are/*");
        boolean matched3 = HelperFunction.checkPatternMatch("/hey", "/*");
        assertTrue(matched1);
        assertTrue(matched2);
        assertTrue(matched3);

    }

    @Test
    public void testIsValidHeadersForInvalid() {
        boolean exception_thrown = false;
        try{
            HelperFunction.isValidHeaders(invalidHeaders);
        } catch (HaltException h) {
            exception_thrown = true;
        }
        assertTrue(exception_thrown);

    }

    @Test
    public void testIsValidHeadersForValid() {
        boolean exception_thrown = false;
        try{
            HelperFunction.isValidHeaders(validHeaders);
        } catch (HaltException h) {
            exception_thrown = true;
        }
        assertFalse(exception_thrown);

    }

    @Test
    public void testCheckPatternMatch() {
        assertTrue(HelperFunction.checkPatternMatch("/hey/how/are/you", "/hey/*/*/you"));
        assertTrue(HelperFunction.checkPatternMatch("/name/id", "/name/*"));
        assertTrue(HelperFunction.checkPatternMatch("/random", "/*"));
        assertFalse(HelperFunction.checkPatternMatch("/random/hey", "/*"));
    }

    @Test
    public void testExtractCookie() {
        assertTrue(HelperFunction.extractCookie("a=b;c=d").get("a").equalsIgnoreCase("b"));
        assertTrue(HelperFunction.extractCookie("a=b;c=d").get("c").equalsIgnoreCase("d"));

    }
}
