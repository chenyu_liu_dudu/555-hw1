package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.m1.helper.HttpParsing;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class TestHttpParsing {


    String sampleXXXRequest =
            "GET /xxx HTTP/1.1\r\n" +
                    "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                    "host: localhost:45555\r\n" +
                    "Accept-Language: en-us\r\n" +
                    "Accept-Encoding: gzip, deflate\r\n" +
                    "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
                    "Connection: Keep-Alive\r\n\r\n";


    @Test
    public void testCorrectUri() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket socket = TestHelper.getMockSocket(
                sampleXXXRequest,
                byteArrayOutputStream);

        Map<String, String> headers = new HashMap<>();
        Map<String, List<String>> parms = new HashMap<>();
        ArrayList<String> body = new ArrayList<>();
        String uri = null;
        try {
            InputStream socketInputStream = socket.getInputStream();
            uri = HttpParsing.parseRequest(socket.getRemoteSocketAddress().toString(), socketInputStream, headers, parms, body);
        } catch (IOException e) { }
        assertTrue(uri.equalsIgnoreCase("/xxx"));
    }



    @Test
    public void testCorrectHost() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket socket = TestHelper.getMockSocket(
                sampleXXXRequest,
                byteArrayOutputStream);

        Map<String, String> headers = new HashMap<>();
        Map<String, List<String>> parms = new HashMap<>();
        ArrayList<String> body = new ArrayList<>();

        try {
            InputStream socketInputStream = socket.getInputStream();
            HttpParsing.parseRequest(socket.getRemoteSocketAddress().toString(), socketInputStream, headers, parms, body);
        } catch (IOException e) { }
        assertTrue(headers.get("host").equalsIgnoreCase("localhost:45555"));
    }


}
