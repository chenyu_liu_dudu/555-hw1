package edu.upenn.cis.cis455.m2.server;

import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.m1.handling.HttpRequest;
import edu.upenn.cis.cis455.m1.handling.HttpResponse;
import edu.upenn.cis.cis455.m1.server.HttpWorker;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import org.apache.logging.log4j.Level;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;


public class TestSendResponse {
    WebService ws = new WebService();

    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
//        edu.upenn.cis.cis455.m1.server.WebService.static_file_location = s + "/www";
    }

    String sampleControlRequest =
            "GET /control HTTP/1.1\r\n" +
                    "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                    "host: localhost:45555\r\n" +
                    "Accept-Language: en-us\r\n" +
                    "Accept-Encoding: gzip, deflate\r\n" +
                    "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
                    "Connection: Keep-Alive\r\n\r\n";
    String samplehomeRequest =
            "GET / HTTP/1.1\r\n" +
                    "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                    "host: localhost:45555\r\n" +
                    "Accept-Language: en-us\r\n" +
                    "Accept-Encoding: gzip, deflate\r\n" +
                    "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
                    "Connection: Keep-Alive\r\n\r\n";


    String sampledirRequest =
            "GET /messy HTTP/1.1\r\n" +
                    "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                    "host: localhost:45555\r\n" +
                    "Accept-Language: en-us\r\n" +
                    "Accept-Encoding: gzip, deflate\r\n" +
                    "Cookie: name1=value1; name2=value2; name3=value3\r\n" +
                    "Connection: Keep-Alive\r\n\r\n";


    @Test
    public void testGetIndexPage() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
                samplehomeRequest,
                byteArrayOutputStream);
        HttpRequest request = new HttpRequest(s);
        HttpResponse response = new HttpResponse();
        try{
            Object body = ws.route_table.get("/getFile").get("GET").handle(request, response);
            assertTrue(response.getStartLine(request.protocol()).startsWith("HTTP/1.1 200"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void TestFindBestMatchRoute()  throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
                sampleControlRequest,
                byteArrayOutputStream);
        HttpRequest request = new HttpRequest(s);
        HttpResponse response = new HttpResponse();
        Route r = HttpWorker.findBestMatchRoute(request.uri(), request.requestMethod(), request);
        try {
            Object body = r.handle(request, response);
            assertTrue(response.getStartLine(request.protocol()).startsWith("HTTP/1.1 200"));
        } catch (Exception e) {

        }

    }



}
