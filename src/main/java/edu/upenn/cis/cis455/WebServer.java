package edu.upenn.cis.cis455;

import static edu.upenn.cis.cis455.SparkController.*;

import org.apache.logging.log4j.Level;

import java.io.File;

/**
 * Initialization / skeleton class.
 * Note that this should set up a basic web server for Milestone 1.
 * For Milestone 2 you can use this to set up a basic server.
 * 
 * CAUTION - ASSUME WE WILL REPLACE THIS WHEN WE TEST MILESTONE 2,
 * SO ALL OF YOUR METHODS SHOULD USE THE STANDARD INTERFACES.
 * 
 * @author zives
 *
 */
public class WebServer {
    public static void main(String[] args) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);

        // TODO: make sure you parse *BOTH* command line arguments properly
        if (args.length == 2) {
            port(Integer.parseInt(args[0]));
            File f = new File(args[1]);
            if (f.isDirectory()) {
                staticFileLocation(f.getAbsolutePath());
            }
        }

//        after("/control", (request, response) -> halt(501, "501 msg"));
//        after("/", (request, response) -> halt(401,"401 msg"));
        before("/hello", (request, response) -> {

            response.redirect("/control", 305 );
        });
        get("/hello", (request, response) -> {
            response.cookie("testCookie4", "4");
            response.removeCookie("testCookie4", "4");
//            response.redirect("/control", 302);
            return "hello";
        });

        get("/create1", (request, response) -> {
            request.session(true);
            return "created";
        });

        get("/create2", (request, response) -> {
            request.session().invalidate();
            return "invalidate";
        });

        get("/create2", (request, response) -> {
            if(request.session(false) == null) {
                return "session is null";
            }
            return "session not null";
        });


        get("/s1", (request, response) -> {
            request.session(true).attribute("Attribute1", "Value");
            return "s1";
        });

        get("/s2", (request, response) -> {
            request.session(false).attribute("Attribute2", 1);

            return "s2";
        });

        get("/s3", (request, response) -> {
            request.session().attribute("Attribute1");
            request.session().attribute("Attribute2");

            return "1." + request.session().attribute("Attribute1") + "\n2."+
            request.session().attribute("Attribute2");
        });
        post("/", (request, response) -> {
            return "posting";
        });

//        after("/hello", (request, response) -> {
//            halt(401, "401 msg");
//        });

        System.out.println("Waiting to handle requests!");
        awaitInitialization();
//        before("/", (request, response) -> halt(401, "haltmessage"));
    }
}
