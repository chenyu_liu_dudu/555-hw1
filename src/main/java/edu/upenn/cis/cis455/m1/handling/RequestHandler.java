package edu.upenn.cis.cis455.m1.handling;

import edu.upenn.cis.cis455.m1.ControlPage.DirectoryListingGenerator;
import edu.upenn.cis.cis455.m1.helper.HelperFunction;
import edu.upenn.cis.cis455.m2.server.WebService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class RequestHandler {
    final static Logger logger = LogManager.getLogger(RequestHandler.class);

    final static String url = "http://localhost:" + WebService.port;

    /*
    generate Directory listing response
     */

    public static String generateDirectoryListingResponse(HttpRequest request, HttpResponse response) {
        DirectoryListingGenerator directoryListingGenerator =
                new DirectoryListingGenerator(WebService.static_file_location + request.uri(),
                        request.uri());
        String page =  directoryListingGenerator.generatePage();
        response.header("Content-Length", Integer.toString(page.getBytes().length));
        response.bodyRaw( page.getBytes());
        response.contentType("text/html");
        return page;
    }

    public static String generateFile(String filePath, HttpResponse response) {

        String page = HelperFunction.readAllBytesJava7(filePath);
        response.header("Content-Length", Integer.toString(page.getBytes().length));
        response.bodyRaw( page.getBytes());
        response.contentType("text/html");
        return page;
    }

    public static String generateShutdown(HttpResponse response) {
        String page = HelperFunction.shutdownpage;
        response.header("Content-Length", Integer.toString(page.getBytes().length));
        response.bodyRaw( page.getBytes());
        response.contentType("text/html");
        return page;
    }

    /*
    generate get response
     */
    public static String generateGetResponse(HttpRequest request, HttpResponse response) {
        logger.info("generateGetResponse");

        FileHandler fileHandler = new FileHandler(WebService.static_file_location + request.uri());
        byte[] response_body = null;
        int status_code;
        boolean pathValid = fileHandler.locateFilePathCheckValid();
        if (pathValid) { status_code = 200; }
        else { status_code = 404; }
        //Get all input from header
        InputStream inputStream = fileHandler.getFileInputStream();
        try{
            response_body = inputStream.readAllBytes();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        Path path = new File(fileHandler.filePath).toPath();
        String mimeType = "text/html";
        try{
            mimeType = Files.probeContentType(path);
        } catch (IOException e) {}
        response.bodyRaw(response_body);
        String body_string = new String(response_body, StandardCharsets.UTF_8);
        logger.info("response body length = " + response_body.length);

        response.header("Content-Type", mimeType);
        response.header("Content-Length", String.valueOf(response_body.length));
        response.statusCode(status_code);
        response.contentType(mimeType);

        return body_string;
    }

    /*
    generate Head response
     */
    public static String generateHEADResponse(HttpRequest request, HttpResponse response) {

        String bodyString = generateGetResponse(request, response);
        response.bodyRaw(null);
        return "";

    }
}
