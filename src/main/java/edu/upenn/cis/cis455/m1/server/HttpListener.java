package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.net.Socket;

import edu.upenn.cis.cis455.m2.server.WebService;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
/**
 * Stub for your HTTP server, which listens on a ServerSocket and handles
 * requests
 */
public class HttpListener extends Thread {
    static final Logger logger = LogManager.getLogger(HttpListener.class);

    public HttpListener() {

    }

    public void run() {

        while (WebService.run) { // while shutdown flag is false - accept connection
            Socket clientSocket;
            try{
                clientSocket = WebService.serverSocket.accept();
            } catch (IOException e) {
                continue;
            }
            synchronized (WebService.task_queue) {
                WebService.task_queue.add(new HttpTask(clientSocket));
                WebService.task_queue.notify();
            }
        }



    }


}
