package edu.upenn.cis.cis455.m1.ControlPage;

import java.io.File;
import java.util.Date;

/*
DirectoryListingGenerator generates the directory listing in the root directory
For example
http://localhost:45555/messy
will list all content in "messy" directory
 */
public class DirectoryListingGenerator {
    public String directory = "";
    public String url = "";
    static String table_style = "#thread_table {\n" +
            "  font-family: Arial, Helvetica, sans-serif;\n" +
            "  border-collapse: collapse;\n" +
            "  width: 100%;\n" +
            "}\n" +
            "\n" +
            "#thread_table td, #thread_table th {\n" +
            "  border: 1px solid #ddd;\n" +
            "  padding: 8px;\n" +
            "}\n" +
            "\n" +
            "#thread_table tr:nth-child(even){background-color: #f2f2f2;}\n" +
            "\n" +
            "#thread_table tr:hover {background-color: #ddd;}\n" +
            "\n" +
            "#thread_table th {\n" +
            "  padding-top: 12px;\n" +
            "  padding-bottom: 12px;\n" +
            "  text-align: left;\n" +
            "  background-color: #4CAF50;\n" +
            "  color: white;\n" +
            "}";
    static String heading_stype = "h1 {\n" +
            "  color: green;\n" +
            "}";
    static String icon_style = "<link href=\"//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css\" rel=\"stylesheet\">\n" +
            "<link href=\"//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css\" rel=\"stylesheet\">";

    private String lower = " </table>\n" +
            "   </body>\n" +
            "   \n" +
            "</html>";
    public DirectoryListingGenerator(String directory, String url) {
        this.directory = directory;
        this.url = url;
    }
    public String generatePage() {
        String body = "";
        File f = new File(this.directory);
        File[] files = f.listFiles();

        for (File file:files){
            String modifiedDate =  new Date(file.lastModified()).toString();
            String icon = "<i class=\"icon-folder-close\"></i>\n";
            if (!file.isDirectory()) icon = "<i class=\"icon-file\"></i>\n";

            body += "  <tr>\n" +
                    "            <td>"+ icon +  "<a href="+ this.url + "/" + file.getName()+">"+file.getName()+"</a>\n" +"</td>\n" +
                    "            <td>"+ modifiedDate+ "</td>\n" +
                    "            <td>"+ file.length() +" bytes" + "</td>\n" +
                    "  </tr>\n";
        }

        String upper = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "   <head>\n" +
                icon_style +
                "<style>\n" + table_style + heading_stype + "</style>" +
                "      <title>HTML Table Header</title>\n" +
                "   </head>\n" +
                "\t\n" +
                "   <body>\n" +
                "<h1>"+ this.url + "</h1>\n"+
                "      <table id = \"thread_table\">\n" +
                "         <tr>\n" +
                "            <th>Name</th>\n" +
                "            <th>Last modified</th>\n" +
                "            <th>Size</th>\n" +
                "         </tr>\n";
        return upper + body +lower;

    }
}
