package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.FileHandler;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.handling.HttpRequest;
import edu.upenn.cis.cis455.m1.handling.HttpResponse;
import edu.upenn.cis.cis455.m1.helper.BlockingQueue;
import edu.upenn.cis.cis455.m1.helper.HelperFunction;
import edu.upenn.cis.cis455.m2.interfaces.Filter;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.server.WebService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;

import static edu.upenn.cis.cis455.m1.helper.GlobalVariables.GENERAL_FILTER;

/**
 * Stub class for a thread worker that handles Web requests
 */
public class HttpWorker extends Thread {

    static final Logger logger = LogManager.getLogger(HttpWorker.class);
    public BlockingQueue<HttpTask> taskQueue;
    public String uri;
    public HttpWorker(BlockingQueue queue) {
        this.taskQueue = queue;
    }

    @Override
    public void run() {
        while(WebService.run ) {
            HttpTask task = null;
            synchronized (taskQueue) {
                if(!taskQueue.isEmpty()) {
                    task = taskQueue.poll();
                } else {
                    try {
                        this.uri = null;
                        taskQueue.wait();
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage());
                    }
                }
            }
            if (task !=null) {
                Socket socket = task.getSocket();
                HttpRequest request = null;
                try {
                    request = new HttpRequest(socket);
                    HttpResponse response = new HttpResponse();

                    this.uri = request.uri();
                    logger.info("request uri=" + request.uri());
                    applyFilters(request, response, true);                      //call before_filter
                    Route r = null;
                    //check if uri matches directory listing
                    if (FileHandler.isfileValid(WebService.static_file_location + request.uri())
                            && !request.requestMethod().equals("POST")) {
                        logger.info("isfileValid");
                        r = WebService.route_table.get("/getFile").get(request.requestMethod());
                        Object response_body = r.handle(request, response);
                        if(response.bodyRaw() == null) {
                            response.body(response_body.toString());
                        }
                        applyFilters(request, response, false);                        //call after_filter
                        sendResponse(socket,request, response);
                        continue;
                    }
                    else if (FileHandler.isDirectoryNoIndex(WebService.static_file_location + request.uri())
                            && !request.requestMethod().equals("POST")) {
                        logger.info("isDirectoryWithNOIndex");
                        r = WebService.route_table.get("/dlist").get(request.requestMethod());
                        Object response_body = r.handle(request, response);
                        if(response.bodyRaw() == null) {
                            response.body(response_body.toString());
                        }
                        applyFilters(request, response, false);                        //call after_filter
                        sendResponse(socket,request, response);
                        continue;
                    }
                    else if(FileHandler.isDirectoryWithIndex(WebService.static_file_location + request.uri())
                            && !request.requestMethod().equals("POST")) {
                        logger.info("isDirectoryWithIndex");
                        r = WebService.route_table.get("/getIndexFile").get(request.requestMethod());
                        Object response_body = r.handle(request, response);
                        if(response.bodyRaw() == null) {
                            response.body(response_body.toString());
                        }
                        applyFilters(request, response, false);                        //call after_filter
                        sendResponse(socket,request, response);
                        continue;
                    }
                    //find the best matching route
                    else {
                        r = findBestMatchRoute(request.uri(), request.requestMethod(), request);
                    }
                    Object response_body = null;
                    if (r != null) { //valid path is found
                        response_body = r.handle(request, response);
                        if(response.bodyRaw() == null &&
                                !request.requestMethod().equals("HEAD")) {
                            response.body(response_body.toString());
                        }
                        populateResponseSessionID(request, response);
                    }
                    //invalid path
                    else {
                        r = WebService.route_table.get("/404.html").get("GET");
                        response_body = r.handle(request, response);
                        if(response.bodyRaw() == null) {
                            response.body(response_body.toString());
                        }
                    }
                    applyFilters(request, response, false);                        //call after_filter
                    if (!socket.isClosed()) {
                        sendResponse(socket,request, response);
                    }
                } catch (HaltException e) {
                    logger.info("catch halt exception");
                    HttpIoHandler.sendException(socket, request,  e);
                } catch (Exception ee) {

                }
            }
        }
    }


    public static boolean sendResponse(Socket socket, HttpRequest request, HttpResponse httpResponse) {
        DataOutputStream out;
        try {
            out = new DataOutputStream(socket.getOutputStream());
            out.writeBytes(httpResponse.getStartLine(request.protocol()));
            out.writeBytes(httpResponse.getHeaders());
            if(httpResponse.getBody() != null) {
                out.write(httpResponse.getBody());
            }
            out.flush();
            out.close();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
        return true;
    }

    public static void populateResponseSessionID(HttpRequest request, HttpResponse response) {
        if(request.session()!=null && request.sessionUpdated) {
            response.cookie("JSESSIONID", request.session().id());
        }
    }

    public static Route findBestMatchRoute(String uri, String requestMethod, HttpRequest request) {
        try {
            return WebService.route_table
                    .get(request.uri())
                    .get(request.requestMethod());
        } catch (Exception e) {
            String[] uriToken = uri.split("/");
            int bestMatch = 0;
            Route bestMatchRoute = null;
            for(String path: WebService.route_table.keySet()) {
                String[] path_arr = path.split("/");
                int curMatch = 0;
                boolean noMatch = false;
                if(path_arr.length == uriToken.length) {
                    for(int i=0;i<path_arr.length;i++) {
                        if (uriToken[i].equalsIgnoreCase(path_arr[i])) curMatch ++;
                        else if(!path_arr[i].equals("*") && !path_arr[i].startsWith(":")) {
                            noMatch = true;
                            break;
                        }
                    }
                    if(noMatch) continue;
                    else if (curMatch > bestMatch
                            && WebService.route_table.get(path).containsKey(requestMethod)) {
                        bestMatch = curMatch;
                        bestMatchRoute = WebService.route_table.get(path).get(requestMethod);
                    }
                }
            }
            return bestMatchRoute;
        }
    }


    public static void applyFilters(HttpRequest request, HttpResponse response, boolean is_before_filter) throws HaltException {
        Map<String, ArrayList<Filter>> target_filter_map = is_before_filter ? WebService.before_filters : WebService.after_filters;
        try {
            if (target_filter_map.containsKey(request.uri())) {
                //apply specified filters
                logger.info("applying "+request.uri()+"  filter");
                for (Filter f : target_filter_map.get(request.uri())) {
                    f.handle(request, response);
                }
            }
            //apply general filters
            if (target_filter_map.containsKey(GENERAL_FILTER)) {
                for (Filter f :target_filter_map.get(GENERAL_FILTER)) {
                    f.handle(request, response);
                }
            }
            //apply optionally take a pattern filters
            if(!findMatchingFilter(request.uri(), is_before_filter).isEmpty()) {
                for (Filter f : findMatchingFilter(request.uri(), is_before_filter)) {
                    f.handle(request, response);
                }
            }
        } catch (HaltException e) {
            logger.info("throwing halt exception");
            throw e;
        } catch (Exception ee) {

        }
    }


    public static ArrayList<Filter> findMatchingFilter(String uri, boolean find_before) {
        ArrayList<Filter> filter_list = new ArrayList<>();
        Map<String, ArrayList<Filter>> target_map = find_before ? WebService.before_filters : WebService.after_filters;
        for (String path : target_map.keySet()) {
            if(HelperFunction.checkPatternMatch(uri, path)) {
                filter_list.addAll(target_map.get(path));
            }
        }
        return filter_list;

    }

    public static void putHandler(HttpRequest request) {
        String fileName = WebService.static_file_location + request.uri().substring(0, request.uri().lastIndexOf("/"));
        Path path = Paths.get(fileName);
        try {
            Files.createDirectories(path);
            FileOutputStream outputStream = new FileOutputStream(WebService.static_file_location + request.uri());
            byte[] strToBytes = request.body().getBytes();
            outputStream.write(strToBytes);
            outputStream.close();
        } catch (IOException e) {
            logger.error("write error =" + e.getLocalizedMessage());
        }
    }

    public static void deleteHandler(HttpRequest request) {
        File file = new File(WebService.static_file_location + request.uri());
        logger.info(file.getAbsolutePath());
        if(file.delete())
        {
            logger.info("file deleted");
        }
        else
        {
            logger.info("failed to delete file");
        }
    }

    public static void optionHandler(HttpRequest request, HttpResponse response) {
        ArrayList<String> allowMethods = new ArrayList<>();
        for(String m: WebService.route_table.get(request.uri()).keySet()){
            allowMethods.add(m);
        }
        response.statusCode(204);
        response.headers.remove("Content-Type");
        response.bodyRaw(null);
        response.body((String)null);
        response.header("Allow", String.join(",", allowMethods));
    }
}
