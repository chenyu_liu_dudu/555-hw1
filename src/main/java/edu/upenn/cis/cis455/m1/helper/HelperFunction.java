package edu.upenn.cis.cis455.m1.helper;

import edu.upenn.cis.cis455.exceptions.HaltException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class HelperFunction {
    final static Logger logger = LogManager.getLogger(HelperFunction.class);

    public static String getCurrentDate() {
        //Date: Fri, 31 Dec 1999 23:59:59 GMT
        SimpleDateFormat formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss zzz");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    public static String getDate(long offset) {
        //Date: Fri, 31 Dec 1999 23:59:59 GMT
        SimpleDateFormat formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss zzz");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(offset);
    }

    public static Date parseDateString(String t) {
        if (t==null) return null;
        SimpleDateFormat sf1 = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss zzz");
        SimpleDateFormat sf2 = new SimpleDateFormat("EEEEEEEEE, dd-MMM-yy HH:mm:ss zzz");
        SimpleDateFormat sf3 = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");
        sf1.setTimeZone(TimeZone.getTimeZone("GMT"));
        sf2.setTimeZone(TimeZone.getTimeZone("GMT"));
        sf3.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = null;
        try{
            date = sf1.parse(t);
        } catch (ParseException e) {

        }
        try{
            date = sf2.parse(t);
        } catch (ParseException e) {

        }
        try{
            date = sf3.parse(t);
        } catch (ParseException e) {

        }
        return date;
    }

    public static Map<String, String> extractCookie(String str) {
        if (str == null) return null;
        Map<String, String > cookies = new HashMap<>();
        String[] arr = str.split(";");

        for (String s : arr) {
            if (!s.isEmpty()) {
                String[] tokens = s.split("=");
                cookies.put(tokens[0], tokens[1]);
            }

        }
        return cookies;
    }


    public static boolean checkPatternMatch(String uri, String path) {
        String[] uriToken = uri.split("/");
        String[] pathToken = path.split("/");
        if(uriToken.length == pathToken.length) {
            for(int i=0;i<uriToken.length; i++) {
                if(!uriToken[i].equalsIgnoreCase(pathToken[i]) && !pathToken[i].equalsIgnoreCase("*")) {
                    return false;
                }
            }
            return true;
        }
        else {
            return false;
        }

    }

    public static String readAllBytesJava7(String filePath)
    {
        String content = "";

        try
        {
            content = new String ( Files.readAllBytes( Paths.get(filePath) ) );
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return content;
    }

    public static void isValidHeaders(Map<String, String> headers) throws HaltException {
        for (String h: headers.keySet()) {
            if (!GlobalVariables.HEADERS.contains(h.toLowerCase())) {
                logger.info("invalid header = " + h + " " +headers.get(h));
                throw new HaltException(400);
            }
        }

    }

    public static String shutdownpage = "<!doctype html>\n" +
            "<html lang=\"en\">\n" +
            "\n" +
            "<head>\n" +
            "  <meta charset=\"utf-8\">\n" +
            "  <title>Page Not Found</title>\n" +
            "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "  <style>\n" +
            "    * {\n" +
            "      line-height: 1.2;\n" +
            "      margin: 0;\n" +
            "    }\n" +
            "\n" +
            "    html {\n" +
            "      color: #888;\n" +
            "      display: table;\n" +
            "      font-family: sans-serif;\n" +
            "      height: 100%;\n" +
            "      text-align: center;\n" +
            "      width: 100%;\n" +
            "    }\n" +
            "\n" +
            "    body {\n" +
            "      display: table-cell;\n" +
            "      vertical-align: middle;\n" +
            "      margin: 2em auto;\n" +
            "    }\n" +
            "\n" +
            "    h1 {\n" +
            "      color: #555;\n" +
            "      font-size: 2em;\n" +
            "      font-weight: 400;\n" +
            "    }\n" +
            "\n" +
            "    p {\n" +
            "      margin: 0 auto;\n" +
            "      width: 280px;\n" +
            "    }\n" +
            "\n" +
            "    @media only screen and (max-width: 280px) {\n" +
            "\n" +
            "      body,\n" +
            "      p {\n" +
            "        width: 95%;\n" +
            "      }\n" +
            "\n" +
            "      h1 {\n" +
            "        font-size: 1.5em;\n" +
            "        margin: 0 0 0.3em;\n" +
            "      }\n" +
            "\n" +
            "    }\n" +
            "  </style>\n" +
            "</head>\n" +
            "\n" +
            "<body>\n" +
            "  <h1>..... Server Shutting Down .....</h1>\n" +
            "  <p>Goodbye</p>\n" +
            "</body>\n" +
            "\n" +
            "</html>\n" +
            "<!-- IE needs 512+ bytes: https://docs.microsoft.com/archive/blogs/ieinternals/friendly-http-error-pages -->\n" +
            "\n";
}
