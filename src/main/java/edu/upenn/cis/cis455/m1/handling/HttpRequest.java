package edu.upenn.cis.cis455.m1.handling;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.helper.HelperFunction;
import edu.upenn.cis.cis455.m1.helper.HttpParsing;
import edu.upenn.cis.cis455.m1.server.HttpWorker;
import edu.upenn.cis.cis455.m2.HttpSession;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Session;
import edu.upenn.cis.cis455.m2.server.WebService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.*;

public class HttpRequest extends Request {
    final static Logger logger = LogManager.getLogger(HttpRequest.class);
    private String requestMethod;
    private String host;
    private String userAgent;
    private int port;
    private String pathInfo;
    private String url;
    private String uri;
    private String rawUri;
    private String protocol;
    private String contentType;
    private String ip;
    private String body;
    private int contentLength;
    private Map<String,String> headers;
    private Map<String, List<String>> qParams;
    private Map<String, String > params = new HashMap<>();
    private Date ifModifiedSince;
    private Date ifUnmodifiedSince;
    private Map<String, String> cookies = new HashMap<>();
    private HttpSession session;
    private Map<String, Object> attributes = new HashMap<>();
    public boolean sessionUpdated = false;

    public HttpRequest(Socket socket) {
        Map<String, String> headers = new HashMap<>();
        Map<String, List<String>> queryParms = new HashMap<>();
        ArrayList<String> body = new ArrayList<>();
        try {
            InputStream socketInputStream = socket.getInputStream();
            this.rawUri = HttpParsing.parseRequest(socket.getRemoteSocketAddress().toString(), socketInputStream, headers, queryParms, body);
            if (rawUri.contains("?")) this.uri = rawUri.substring(0, rawUri.indexOf("?"));
            else if (rawUri.startsWith("http")) {
                rawUri = this.rawUri.substring(rawUri.indexOf("//")+2);
                int second_slash = rawUri.indexOf("/");
                this.uri = rawUri.substring(second_slash);
            }
            else this.uri = rawUri;
            findRequestParams(this.uri);
        }
        catch (IOException e) {
        }
        populateHeaders(headers);

        if(headers.containsKey("expect")) {
            try{
                OutputStream s = socket.getOutputStream();
                String cont = this.protocol() + " 100 Continue\r\n\r\n";
                s.write(cont.getBytes());
                s.flush();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }

        if(headers.containsKey("if-modified-since") && requestMethod.equals("GET")) {
            FileHandler fileHandler = new FileHandler(WebService.static_file_location + uri);
            if(!fileHandler.locateFilePathCheckValid()) {throw new HaltException();};
            if (fileHandler.last_modified.before(this.ifModifiedSince)) {
                HttpResponse response = new HttpResponse();
                response.statusCode(304);
                response.contentType(null);
                response.bodyRaw(null);
                HttpWorker.sendResponse(socket, this, response);
            }
        }
        if (headers.containsKey("if-unmodified-since")) {
            FileHandler fileHandler = new FileHandler(WebService.static_file_location + uri);
            if(!fileHandler.locateFilePathCheckValid()) {throw new HaltException();};
            if (fileHandler.last_modified.after(ifUnmodifiedSince)) {
                HttpResponse response = new HttpResponse();
                response.statusCode(412);
                response.contentType(null);
                response.bodyRaw(null);
                HttpWorker.sendResponse(socket, this, response);
            }
        }

        this.headers = headers;
        if (body.size()>0) {
            this.body = body.get(0);
        }
        if(this.body !=null
                && this.requestMethod.equals("POST")) {
            Map<String, List<String>> postBodyParams = new HashMap<>();
            String[] body_token = this.body.split("&");
            for(String b:body_token) {
                String[] kv = b.split("=");
                if(postBodyParams.containsKey(kv[0])) { postBodyParams.get(kv[0]).add(kv[1]); }
                else { postBodyParams.put(kv[0], new ArrayList<>(){{add(kv[1]);}}); }
            }
            this.qParams = postBodyParams;
        }
        else { this.qParams = queryParms; }
    }

    @Override
    public String requestMethod() {
        return requestMethod;
    }

    @Override
    public String host() {
        return host;
    }

    @Override
    public String userAgent() {
        return userAgent;
    }

    @Override
    public int port() {
        return port;
    }

    @Override
    public String pathInfo() {
        return pathInfo;
    }

    @Override
    public String url() {
        return url;
    }

    @Override
    public String uri() {
        return uri;
    }

    @Override
    public String protocol() {
        return protocol;
    }

    @Override
    public String contentType() {
        return contentType;
    }

    @Override
    public String ip() {
        return ip;
    }

    @Override
    public String body() {
        return body;
    }

    @Override
    public int contentLength() {
        return contentLength;
    }

    @Override
    public String headers(String name) {
        return headers.get(name);
    }

    @Override
    public Set<String> headers() {
        Set<String> set = new HashSet<>(headers.keySet());
        return set;
    }

    public Date getIfModifiedSince() {
        return this.ifModifiedSince;
    }

    public Date getIfUnmodifiedSince() {
        return this.ifUnmodifiedSince;
    }

    /*
        get current session if session exist and valid
        otherwise create a new session
     */
    @Override
    public Session session() {
        if(this.session!=null) {
            this.session.access();
            if(this.session.is_valid) {
                return this.session;
            }
        }
        return session(true);
    }

    /*
    create a new session if session not exist or expired
     */
    @Override
    public Session session(boolean create) {
        HttpSession session;

        //create a session and put into session map
        if (!create) {
            String sessionId = this.cookies.get("JSESSIONID");
            if(sessionId!=null) {
                session = WebService.session_map.getOrDefault(sessionId, null);
                if(session==null) return null;
                return session.is_valid ? session : null;
            }
            else {
                return null;
            }
        }
        if (this.cookies != null && this.cookies.containsKey("JSESSIONID")) {
            String sessionId = this.cookies.get("JSESSIONID");
            session = WebService.session_map.getOrDefault(sessionId, null);
            if (session != null) {
                session.access();
                if (session.is_valid) {
                    this.session = session;
                    return this.session;
                }
            }
            else {
                session = new HttpSession();
            }
        }
        else {
            session = new HttpSession();
        }
        this.session = session;
        WebService.session_map.put(session.id(), session);
        sessionUpdated = true;
        return this.session;
    }

    @Override
    public Map<String, String> params() {
        return this.params;
    }

    @Override
    public String queryParams(String param) {
        return this.qParams.getOrDefault(param, null).get(0);
    }

    @Override
    public List<String> queryParamsValues(String param) {
        return this.qParams.getOrDefault(param, null);
    }

    @Override
    public Set<String> queryParams() {
        return this.qParams.keySet();
    }

    @Override
    public String queryString() {
        if (this.rawUri.contains("?")) {
            return this.rawUri.substring(this.rawUri.indexOf("?")+1);
        }
        return null;
    }

    @Override
    public void attribute(String attrib, Object val) {
        this.attributes.put(attrib, val);
    }

    @Override
    public Object attribute(String attrib) {
        return this.attributes.getOrDefault(attrib, null);
    }

    @Override
    public Set<String> attributes() {
        return this.attributes.keySet();
    }

    @Override
    public Map<String, String> cookies() {
        return this.cookies;
    }

    public boolean findRequestParams(String uri) {
        String[] uri_token = uri.split("/");
        for(String path: WebService.route_table.keySet()) {
            String[] path_token = path.split("/");
            if (isMatch(uri_token, path_token)) {
                for(int i=0;i<uri_token.length ;i++) {
                    if(path_token[i].startsWith(":")) {
                        this.params.put(path_token[i], uri_token[i]);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public boolean isMatch(String[] uri_token, String[] path_token) {
        if(path_token.length == uri_token.length) {
            for(int i=0;i<path_token.length;i++) {
                if (!path_token[i].equalsIgnoreCase(uri_token[i]) && !path_token[i].startsWith(":")) {
                    return false;
                }
            }
        }
        else {
            return false;
        }
        return true;

    }

    public void populateHeaders(Map<String, String> headers) {
        for(String key: headers.keySet()) {
            switch (key){
                case "content-type" :
                    this.contentType = headers.get("content-type");
                    break;

                case "user-agent" :
                    this.userAgent = headers.get("user-agent");
                    break;
                case "if-modified-since":
                    this.ifModifiedSince = HelperFunction.parseDateString(headers.get("if-modified-since"));
                    break;
                case "if-unmodified-since":
                    this.ifUnmodifiedSince = HelperFunction.parseDateString(headers.get("if-unmodified-since"));
                    break;
                case "cookie":
                    this.cookies = HelperFunction.extractCookie(headers.get("cookie"));
                    break;
                case "host":
                    String host = headers.getOrDefault("host", null);
                    int ind = host.indexOf(":");
                    if (ind != -1) {
                        this.host = host.substring(0, ind);
                        this.port = Integer.parseInt(host.substring(ind+1));
                    }
                    break;
                case "http-client-ip":
                    this.ip = headers.get("http-client-ip");
                    break;
                case "protocolVersion":
                    this.protocol = headers.get("protocolVersion");
                    break;
                case "Method":
                    this.requestMethod = headers.get("Method");
                    break;
            }
        }
    }
}
