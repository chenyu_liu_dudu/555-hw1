/**
 * CIS 455/555 route-based HTTP framework
 * 
 * V. Liu, Z. Ives
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.m1.helper.BlockingQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class WebService {
    final static Logger logger = LogManager.getLogger(WebService.class);

    protected HttpListener listener;
    public static BlockingQueue<HttpTask> task_queue  = new BlockingQueue();
    public static ArrayList<Thread> thread_pool = new ArrayList<>();
    public static int thread_size = 5;
    public static int port;
    public static ServerSocket serverSocket = null;
    public static String ipAddress = "localhost";
    public static String static_file_location = "./www";

    public static boolean run = true;

    /**
     * Launches the Web server thread pool and the listener
     */
    public void start() {
        try {
            serverSocket = new ServerSocket(port, 1500);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        for(int i=0;i < thread_size; i++) {
            Thread worker = new HttpWorker(task_queue);
            thread_pool.add(worker);
            worker.start();
        }
        listener = new HttpListener();
        listener.start();
    }

    /**
     * Gracefully shut down the server
     */
    public void stop() {
        run = false;
        try {
            serverSocket.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        synchronized (task_queue) {
            task_queue.notifyAll();
        }
    }

    /**
     * Hold until the server is fully initialized.
     * Should be called after everything else.
     */
    public void awaitInitialization() {
        logger.info("Initializing server");
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt() {
        throw new HaltException();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode) {
        throw new HaltException(statusCode);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(String body) {
        throw new HaltException(body);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode, String body) {
        throw new HaltException(statusCode, body);
    }

    ////////////////////////////////////////////
    // Server configuration
    ////////////////////////////////////////////

    /**
     * Set the root directory of the "static web" files
     */
    public void staticFileLocation(String directory) {
        static_file_location = directory;
    }

    /**
     * Set the IP address to listen on (default 0.0.0.0)
     */
    public void ipAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Set the TCP port to listen on (default 45555)
     */
    public void port(int port) {
        this.port = port;

    }
    /**
     * Set the size of the thread pool
     */
    public void threadPool(int threads) {
        thread_size = threads;
    }

}
