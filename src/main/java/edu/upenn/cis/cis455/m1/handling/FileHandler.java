package edu.upenn.cis.cis455.m1.handling;

import edu.upenn.cis.cis455.m1.helper.GlobalVariables;
import edu.upenn.cis.cis455.m1.helper.HelperFunction;
import edu.upenn.cis.cis455.m2.server.WebService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class FileHandler {
    final static Logger logger = LogManager.getLogger(FileHandler.class);

    public String filePath;
    public Date last_modified;
    public FileHandler(String filePath) {
        this.filePath = filePath;
    }

    /*
    return inputStream specified by filePath
     */
    public InputStream getFileInputStream() {
        InputStream inputStream = null;
        try {
            File file = new File(this.filePath);
            inputStream = new FileInputStream(file);
            return inputStream;
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        }
        return null;

    }

    /*
    Set this.filePath to the correct document path under different scenarios
    returns false if response is 404, true otherwise
     */
    public boolean locateFilePathCheckValid() {
        Path root = Paths.get(".").normalize().toAbsolutePath();
        Path user = Paths.get(this.filePath).normalize().toAbsolutePath();
        File f = new File(this.filePath);
        logger.info("this.filepath = " + this.filePath);
        if (!user.toString().startsWith(root.toString()) || !f.exists()) { //if file outside of scope
            this.filePath = WebService.static_file_location + GlobalVariables.PAGE.get("404");
            return false;
        } else if(f.isDirectory()) { //if it is directory
            this.filePath = this.filePath + GlobalVariables.PAGE.get("index");
            File index = new File(this.filePath);
            if (!index.exists()) { //if index page does not exist
                logger.debug("index not exist");
                this.filePath = WebService.static_file_location + GlobalVariables.PAGE.get("404"); // if no such file exist
                return false;
            }
        }
        else { //file is inside scope and file exist
            this.filePath = f.getAbsolutePath();
            logger.info("f.getAbsolutePath() = " + f.getAbsolutePath());

        }
        SimpleDateFormat sdf = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss zzz");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        this.last_modified = HelperFunction.parseDateString(sdf.format(f.lastModified()));
        return true;
    }

    public static boolean isfileValid(String filePath) {
        Path root = Paths.get(".").normalize().toAbsolutePath();
        Path user = Paths.get(filePath).normalize().toAbsolutePath();
        File f = new File(filePath);
        if (user.toString().startsWith(root.toString()) && f.exists() && !f.isDirectory()) { //if file outside of scope
            return true;
        }
        return false;
    }
    /*
    check if filePath is a directory with no index.html
     */
    public static boolean isDirectoryNoIndex(String filePath) {
        File index = new File(filePath +GlobalVariables.PAGE.get("index"));
        logger.info("filePath + GlobalVariables.PAGE.get(\"index\") ="+filePath  +GlobalVariables.PAGE.get("index"));
        File direc = new File(filePath);
        return direc.isDirectory() && direc.exists() && !index.exists();
    }
    public static boolean isDirectoryWithIndex(String filePath) {
        File index = new File(filePath +"/"+ GlobalVariables.PAGE.get("index"));
        File direc = new File(filePath);
        logger.info(index.getAbsolutePath());

        return direc.isDirectory() && direc.exists() && index.exists();
    }
}
