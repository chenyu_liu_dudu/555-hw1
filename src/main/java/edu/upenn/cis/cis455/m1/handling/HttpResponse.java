package edu.upenn.cis.cis455.m1.handling;

import edu.upenn.cis.cis455.m1.helper.GlobalVariables;
import edu.upenn.cis.cis455.m1.helper.HelperFunction;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static edu.upenn.cis.cis455.m1.helper.HttpStatusCode.getByValue;


public class HttpResponse extends Response {
    public Map<String, String> headers = new HashMap<>();
    public Map<String, Map<String, String>> cookies = new HashMap<>();
    final static Logger logger = LogManager.getLogger(HttpResponse.class);
    public ArrayList<String> allCookies = new ArrayList<>();
    public String protocol = "HTTP/1.1";

    public HttpResponse() {
        this.statusCode = 200;
        this.headers.put( "Content-Type","text/html");
        this.headers.put("Connection", "close");
        this.headers.put("Date", HelperFunction.getCurrentDate());
        this.contentType ="text/html";
    }

    public String getContentType() { return this.contentType; }
    public byte[] getBody() { return this.body; }
    public int getStatusCode() { return this.statusCode; }
    public String getStartLine(String protocol) {return protocol +" "+ getByValue(this.statusCode);}

    public void statusCode(int statusCode) {
        this.statusCode = statusCode;
    }
    public void body(byte[] body) {
        this.body = body;
    }
    public void contentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public String getHeaders() {
        String header = "";
        for(String s : this.headers.keySet()) {

            header += s + ": " + this.headers.get(s) + "\r\n";
        }
        for(String cookie : this.cookies.keySet()) {
            ArrayList<String> pairs = new ArrayList<>();
            pairs.add(cookie + "=" + this.cookies.get(cookie).get("value"));
            for(String c: this.cookies.get(cookie).keySet()) {
                if (c.equalsIgnoreCase("value")
                        || c.equalsIgnoreCase("Secured")
                        ||c.equalsIgnoreCase("HttpOnly")
                ) continue;
                pairs.add(c + "=" + this.cookies.get(cookie).get(c));
            }
            if (this.cookies.get(cookie).containsKey("Secured") &&
                    this.cookies.get(cookie).get("Secured").equalsIgnoreCase("true")) pairs.add("Secured");
            if (this.cookies.get(cookie).containsKey("HttpOnly") &&
                    this.cookies.get(cookie).get("HttpOnly").equalsIgnoreCase("true")) pairs.add("HttpOnly");
            if (this.cookies.get(cookie).containsKey("Max-Age") &&
            Long.parseLong(this.cookies.get(cookie).get("Max-Age"))>0) {
                Long expires = Long.parseLong(this.cookies.get(cookie).get("Max-Age")) *1000 + new Date().getTime();
                pairs.add("Expires=" + HelperFunction.getDate(expires));
            }
            header += "Set-Cookie:" + String.join(";", pairs) + "\r\n";
        }
        header += "\r\n";
        return header;
    }

    @Override
    public void header(String header, String value) {
        this.headers.put(header, value);
    }

    @Override
    public void redirect(String location) {
        this.header("Location", location);
        this.statusCode = 301;
    }

    @Override
    public void redirect(String location, int httpStatusCode) {
        this.header("Location", location);
        this.statusCode = httpStatusCode;
    }

    @Override
    public void cookie(String name, String value) {

        if(this.cookies.containsKey(name)) {
            this.cookies.get(name).put("value", value);
        }
        else {
            this.cookies.put(name, new HashMap<>() {{
                put("value", value);
            }});
        }
    }

    @Override
    public void cookie(String name, String value, int maxAge) {
        cookie(name, value);
        this.cookies.get(name).put("Max-Age", String.valueOf(maxAge));

    }
    @Override
    public void cookie(String name, String value, int maxAge, boolean secured) {
        cookie(name, value, maxAge);
        this.cookies.get(name).put("Secured", String.valueOf(secured));
    }

    @Override
    public void cookie(String name, String value, int maxAge, boolean secured, boolean httpOnly) {
        cookie(name, value, maxAge, secured);
        this.cookies.get(name).put("HttpOnly", String.valueOf(httpOnly));
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void cookie(String path, String name, String value) {
        String combined_key = path + " " + name;
        if (this.cookies.containsKey(combined_key)) {
            this.cookies.get(combined_key).put("value", value);
        }
        else {
            this.cookies.put(combined_key, new HashMap<>(){{
                put("value", value);
            }});
        }
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge) {
        String combined_key = path + " " + name;
        cookie(path, name, value);
        this.cookies.get(combined_key).put("Max-Age", String.valueOf(maxAge));
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge, boolean secured) {
        String combined_key = path + " " + name;
        cookie(path, name, value, maxAge);
        this.cookies.get(combined_key).put("Secured", String.valueOf(secured));
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge, boolean secured, boolean httpOnly) {
        String combined_key = path + " " + name;
        cookie(path, name, value, maxAge, secured);
        this.cookies.get(combined_key).put("HttpOnly", String.valueOf(httpOnly));
    }

    @Override
    public void removeCookie(String name) {
        this.cookies.remove(name);
    }

    @Override
    public void removeCookie(String path, String name) {
        String combined_key = path + " " + name;
        this.cookies.remove(combined_key);
    }
}
