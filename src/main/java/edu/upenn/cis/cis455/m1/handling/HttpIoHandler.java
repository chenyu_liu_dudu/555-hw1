package edu.upenn.cis.cis455.m1.handling;

import java.io.*;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;


/**
 * Handles marshaling between HTTP Requests and Responses
 */
public class HttpIoHandler {
    final static Logger logger = LogManager.getLogger(HttpIoHandler.class);
    /**
     * Sends an exception back, in the form of an HTTP response code and message.
     * Returns true if we are supposed to keep the connection open (for persistent
     * connections).
     */
    public static boolean sendException(Socket socket, HttpRequest request, HaltException except) {
        if(except != null) {
            HttpResponse response = new HttpResponse();
            response.status(except.statusCode());
            response.body(except.body());
            response.protocol = except.protocol;
            sendResponse(socket, (HttpRequest)request, (HttpResponse) response);
        }
        return true;
    }

    /**
     * Sends data back. Returns true if we are supposed to keep the connection open
     * (for persistent connections).
     */
    public static boolean sendResponse(Socket socket,HttpRequest request, HttpResponse response) {
        DataOutputStream out;
        try {
            out = new DataOutputStream(socket.getOutputStream());
            if (request == null ) {
                out.writeBytes(response.getStartLine(response.protocol));
            }
            else {
                out.writeBytes(response.getStartLine(request.protocol()));
            }
            out.writeBytes(response.getHeaders());
            if(response.getBody()!=null) out.write(response.getBody());
            out.flush();
            out.close();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
        return true;

    }
}
