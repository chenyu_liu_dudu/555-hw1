package edu.upenn.cis.cis455.m1.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GlobalVariables {


    public static Map<String, String> PAGE = new HashMap<String, String>(Map.of(
            "404", "/404.html",
            "index", "index.html"

    ));
    public static final String GENERAL_FILTER = "GF";

    public static final Set<String> HEADERS = Set.of("sec-fetch-site","sec-fetch-mode","sec-fetch-user","upgrade-insecure-requests",
            "sec-fetch-dest","a-im", "max-forwards", "if-range", "connection", "cookie", "host", "upgrade", "accept", "accept-charset",
            "warning", "via", "accept-encoding", "from", "content-length", "date", "pragma", "access-control-request-headers", "origin",
            "access-control-request-method", "if-match", "referer", "if-none-match", "te", "expect", "accept-language", "if-unmodified-since",
            "range", "proxy-authorization", "if-modified-since", "authorization", "content-type", "user-agent", "forwarded", "cache-control",
            "accept-datetime"
    );

    public static final Set<String> METHODS = Set.of("GET", "HEAD", "OPTIONS", "PUT", "POST", "DELETE");

}
