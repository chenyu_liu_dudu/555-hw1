package edu.upenn.cis.cis455.m1.helper;

import java.util.LinkedList;
import java.util.List;

public class BlockingQueue<E> {

    private List<E> queue = new LinkedList();



    public synchronized void add(E item)
             {
        this.queue.add(item);
        if(this.queue.size() == 1) {
            notifyAll();
        }
    }


    public synchronized E poll() {
        while(this.queue.size() == 0){
            try{
                wait();

            }catch (InterruptedException e) {};
        }

        return this.queue.remove(0);
    }
    public synchronized boolean isEmpty() {
        return this.queue.size() == 0;
    }

}
