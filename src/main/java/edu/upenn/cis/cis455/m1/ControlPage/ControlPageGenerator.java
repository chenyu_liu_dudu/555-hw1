package edu.upenn.cis.cis455.m1.ControlPage;

import edu.upenn.cis.cis455.m1.helper.HelperFunction;
import edu.upenn.cis.cis455.m1.server.HttpWorker;
import edu.upenn.cis.cis455.m2.server.WebService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ControlPageGenerator {

    static final Logger logger = LogManager.getLogger(ControlPageGenerator.class);
    static String button = "<a href=\"shutdown\"><button type=\"button\" class=\"button\">Shut Down</button></a>";
    static String button_stype =
            ".button {\n" +
            "  background-color: #4CAF50;\n" +
            "  border: none;\n" +
            "  color: white;\n" +
            "  padding: 15px 32px;\n" +
            "  text-align: center;\n" +
            "  text-decoration: none;\n" +
            "  display: inline-block;\n" +
            "  font-size: 16px;\n" +
            "  margin: 4px 2px;\n" +
            "  cursor: pointer;\n" +
            "}\n" ;
    static String log_style = "#just-line-break {\n" +
            "  white-space: pre-line;\n" +
            "}";
    static String table_style = "#thread_table {\n" +
            "  font-family: Arial, Helvetica, sans-serif;\n" +
            "  border-collapse: collapse;\n" +
            "  width: 100%;\n" +
            "}\n" +
            "\n" +
            "#thread_table td, #thread_table th {\n" +
            "  border: 1px solid #ddd;\n" +
            "  padding: 8px;\n" +
            "}\n" +
            "\n" +
            "#thread_table tr:nth-child(even){background-color: #f2f2f2;}\n" +
            "\n" +
            "#thread_table tr:hover {background-color: #ddd;}\n" +
            "\n" +
            "#thread_table th {\n" +
            "  padding-top: 12px;\n" +
            "  padding-bottom: 12px;\n" +
            "  text-align: left;\n" +
            "  background-color: #4CAF50;\n" +
            "  color: white;\n" +
            "}";
    static String heading = "<h1>Thread Control Table</h1>\n";
    static String heading_stype = "h1 {\n" +
            "  color: green;\n" +
            "}";
    private String upper = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "   <head>\n" +
                "<style>\n" + button_stype+ table_style + heading_stype +log_style+ "</style>" +
            "      <title>HTML Table Header</title>\n" +
            "   </head>\n" +
            "\t\n" +
            "   <body>\n" +
            heading +
            "      <table id = \"thread_table\">\n" +
            "         <tr>\n" +
            "            <th>Thread number</th>\n" +
            "            <th>Thread state</th>\n" +
            "            <th>Url handling</th>\n" +
            "         </tr>\n";

    private String table_end = " </table>\n" +
            button;
    private String lower =
            "   </body>\n" +
            "   \n" +
            "</html>";


    public ControlPageGenerator() {
    }

    public String generatePage() {
        String body = "";
        for(Thread th : WebService.thread_pool) {
            String url = "";
            if (((HttpWorker) th).uri !=null) {
                url = "http://localhost:" + ((HttpWorker) th).uri;
            }
            body += "  <tr>\n" +
                    "            <td>"+ "[" + th.getId() +"]" + "</td>\n" +
                    "            <td>"+ th.getState().toString() + "</td>\n" +
                    "            <td>"+ url + "</td>\n" +
                    "  </tr>\n";

        }
        String log = "<div id=\"just-line-break\">" + HelperFunction.readAllBytesJava7(System.getProperty("user.dir") + "/app.log") + "</div>\"";


        return upper + body + table_end + log+lower;
    }




}
