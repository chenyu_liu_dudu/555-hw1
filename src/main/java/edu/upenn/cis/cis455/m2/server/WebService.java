/**
 * CIS 455/555 route-based HTTP framework
 * 
 * Z. Ives, 8/2017
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.cis455.m2.server;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.m1.ControlPage.ControlPageGenerator;
import edu.upenn.cis.cis455.m1.handling.FileHandler;
import edu.upenn.cis.cis455.m1.handling.HttpRequest;
import edu.upenn.cis.cis455.m1.handling.HttpResponse;
import edu.upenn.cis.cis455.m1.handling.RequestHandler;
import edu.upenn.cis.cis455.m2.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.interfaces.Filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static edu.upenn.cis.cis455.m1.helper.GlobalVariables.GENERAL_FILTER;

public class WebService extends edu.upenn.cis.cis455.m1.server.WebService {
    final static Logger logger = LogManager.getLogger(WebService.class);
    public static Map<String, Map<String, Route>> route_table = new HashMap<>();
    public static Map<String, HttpSession> session_map = new HashMap<>();
    public static Map<String, ArrayList<Filter>> before_filters = new HashMap<>();
    public static Map<String, ArrayList<Filter>> after_filters = new HashMap<>();


    public WebService() {
        super();
        this.route_table.put("/control", new HashMap<>(){{
            put("GET", (request, response) -> {
            ControlPageGenerator controlPageGenerator = new ControlPageGenerator();
            String page =  controlPageGenerator.generatePage();
            response.header("Content-Length", Integer.toString(page.getBytes().length));
            return page;
            });
            put("HEAD", (request, response) -> RequestHandler.generateHEADResponse((HttpRequest)request, (HttpResponse)response));
        }});

        this.route_table.put("/favicon.ico", new HashMap<>() {{
            put("GET", (request, response) ->
                    RequestHandler.generateGetResponse((HttpRequest) request, (HttpResponse) response));
            put("HEAD", (request, response) -> RequestHandler.generateHEADResponse((HttpRequest)request, (HttpResponse)response));

        }});

        this.route_table.put("/shutdown", new HashMap<>() {{
            put("GET", (request, response) -> {
                        SparkController.ws.stop();
                        return RequestHandler.generateShutdown( (HttpResponse) response);
                    });
            put("HEAD", (request, response) -> RequestHandler.generateHEADResponse((HttpRequest)request, (HttpResponse)response));
        }});

        this.route_table.put("/dlist", new HashMap<>(){{
            put("GET", (request, response) ->
                    RequestHandler.generateDirectoryListingResponse((HttpRequest) request, (HttpResponse) response));
            put("HEAD", (request, response) -> RequestHandler.generateHEADResponse((HttpRequest)request, (HttpResponse)response));


        }});

        this.route_table.put("/getIndexFile", new HashMap<>(){{
            put("GET", (request, response) ->
                    RequestHandler.generateFile(WebService.static_file_location + request.uri()+ "/index.html", (HttpResponse) response));
            put("HEAD", (request, response) -> RequestHandler.generateHEADResponse((HttpRequest)request, (HttpResponse)response));

        }});

        this.route_table.put("/getFile", new HashMap<>(){{
            put("GET", (request, response) ->
                    RequestHandler.generateGetResponse((HttpRequest) request, (HttpResponse) response));
            put("HEAD", (request, response) -> RequestHandler.generateHEADResponse((HttpRequest)request, (HttpResponse)response));

        }});

        this.route_table.put("/404.html", new HashMap<>() {{
            put("GET", (request, response) ->
                    RequestHandler.generateFile(WebService.static_file_location + "/404.html", (HttpResponse) response));
            put("HEAD", (request, response) -> RequestHandler.generateHEADResponse((HttpRequest)request, (HttpResponse)response));

        }});




    }



    ///////////////////////////////////////////////////
    // For more advanced capabilities
    ///////////////////////////////////////////////////

    public void get(String path, Route route) {
        //route register
        if (route_table.containsKey(path)) {
            route_table.get(path).put("GET", route);
        }
        else {
            route_table.put(path, new HashMap<>(){{
                put("GET", route);
            }});
        }
    }

    /**
     * Handle an HTTP POST request to the path
     */
    public void post(String path, Route route) {
        if (route_table.containsKey(path)) {
            route_table.get(path).put("POST", route);
        }
        else {
            route_table.put(path, new HashMap<>(){{
                put("POST", route);
            }});
        }
    }

    /**
     * Handle an HTTP PUT request to the path
     */
    public void put(String path, Route route) {
        if (route_table.containsKey(path)) {
            route_table.get(path).put("PUT", route);
        }
        else {
            route_table.put(path, new HashMap<>(){{
                put("PUT", route);
            }});
        }
    }

    /**
     * Handle an HTTP DELETE request to the path
     */
    public void delete(String path, Route route) {
        if (route_table.containsKey(path)) {
            route_table.get(path).put("DELETE", route);
        }
        else {
            route_table.put(path, new HashMap<>(){{
                put("DELETE", route);
            }});
        }
    }

    /**
     * Handle an HTTP HEAD request to the path
     */
    public void head(String path, Route route) {
        if (route_table.containsKey(path)) {
            route_table.get(path).put("HEAD", route);
        }
        else {
            route_table.put(path, new HashMap<>(){{
                put("HEAD", route);
            }});
        }
    }

    /**
     * Handle an HTTP OPTIONS request to the path
     */
    public void options(String path, Route route) {
        if (route_table.containsKey(path)) {
            route_table.get(path).put("OPTIONS", route);
        }
        else {
            route_table.put(path, new HashMap<>(){{
                put("OPTIONS", route);
            }});
        }
    }

    ///////////////////////////////////////////////////
    // HTTP request filtering
    ///////////////////////////////////////////////////

    /**
     * Add filters that get called before a request
     */
    public void before(Filter filter) {
        if (before_filters.containsKey(GENERAL_FILTER)) {
            before_filters.get(GENERAL_FILTER).add(filter);
        }
        else {
            before_filters.put(GENERAL_FILTER, new ArrayList<>(){{
                add(filter);
            }});
        }
    }

    /**
     * Add filters that get called after a request
     */
    public void after(Filter filter) {
        if (after_filters.containsKey(GENERAL_FILTER)) {
            after_filters.get(GENERAL_FILTER).add(filter);
        }
        else {
            after_filters.put(GENERAL_FILTER, new ArrayList<>(){{
                add(filter);
            }});
        }
    }

    /**
     * Add filters that get called before a request
     */
    public void before(String path, Filter filter) {
        if (before_filters.containsKey(path)) {
            before_filters.get(path).add(filter);
        }
        else {
            before_filters.put(path, new ArrayList<>(){{
                add(filter);
            }});
        }
    }

    /**
     * Add filters that get called after a request
     */
    public void after(String path, Filter filter) {
        if (after_filters.containsKey(path)) {
            after_filters.get(path).add(filter);
        }
        else {
            after_filters.put(path, new ArrayList<>(){{
                add(filter);
            }});
        }
    }

}
