package edu.upenn.cis.cis455.m2;

import edu.upenn.cis.cis455.m2.interfaces.Session;
import edu.upenn.cis.cis455.m2.server.WebService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class HttpSession extends Session {

    private String id;
    private long creationTime;
    private long lastAccessedTime;
    private int maxInactiveInterval;
    public boolean is_valid;
    private Map<String, Object> attributes = new HashMap<>();
    final static Logger logger = LogManager.getLogger(HttpSession.class);

    public HttpSession() {
        this.creationTime = new Date().getTime();
        this.id = UUID.randomUUID().toString();
        this.is_valid = true;
        this.lastAccessedTime = new Date().getTime();
        this.maxInactiveInterval = 5;
        WebService.session_map.put(this.id, this);
    }

    @Override
    public String id() {
        access();
        if(is_valid) {
            return this.id;
        }
        return null;
    }

    @Override
    public long creationTime() {
        access();
        if(is_valid) {
            return this.creationTime;
        }
        return 0;
    }

    @Override
    public long lastAccessedTime() {
        if(is_valid) {
            return this.lastAccessedTime;
        }
        return 0;
    }

    @Override
    public void invalidate() {
        if(is_valid) {
            this.is_valid = false;
        }

    }

    @Override
    public int maxInactiveInterval() {
        access();
        if(is_valid) {
            return this.maxInactiveInterval;
        }
        return 0;
    }

    @Override
    public void maxInactiveInterval(int interval) {
        access();
        if(is_valid) {
            this.maxInactiveInterval = interval;
        }
    }

    @Override
    public void access() {
        long now = new Date().getTime();
        if(now - this.lastAccessedTime <= this.maxInactiveInterval * 60 * 1000) {
            this.lastAccessedTime = new Date().getTime();
        }
        else {
            invalidate();
        }
    }

    @Override
    public void attribute(String name, Object value) {
        access();
        if(is_valid) {
            this.attributes.put(name, value);
        }
    }

    @Override
    public Object attribute(String name) {
        access();
        if(is_valid) {
            return this.attributes.get(name);
        }
        return null;
    }

    @Override
    public Set<String> attributes() {
        access();
        if (is_valid){
            return this.attributes.keySet();
        }
        return null;
    }

    @Override
    public void removeAttribute(String name) {
        access();
        if (is_valid) {
            this.attributes.remove(name);
        }
    }
}
